#__autora__ = "Ingrid Guaraca"
#__email__ = "ingrid.guaraca@unl.edu.ec"
# Crear un programa en Python que utilice la librería pygame y todo el código de la "Tarea 3"  para dibujar un plano
# cartesiano y representar gráficamente los objetos Punto y los objetos Rectángulo creados en la tarea anterior.
# La salida del programa debe ser capturada en una imagen (similar a la mostrada al final) y guardada en el mismo
# repositorio gitlab de esta tarea.

import pygame
import math

# Tamaño de pantalla

ancho=1000
alto=800

# Colores

morado=[71,4,107]
mangenta=[190,44,91]
verde=[0,255,0]
naranja=[241,150,27]
turquesa=[106,226,218]
verde=[32,248,5]
centro=[int(ancho/2),int(alto/2)]

# Pantalla
pygame.init()
pantalla=pygame.display.set_mode([ancho,alto])
pantalla.fill(color=(234, 211, 211))

# Modulo geometría y sus clases

class Punto:
    x= 0
    y= 0

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            return 'I'
        elif self.x < 0 and self.y > 0:
            return 'II'
        elif self.x < 0 and self.y < 0:
            return 'III'
        elif self.x > 0 and self.y < 0:
             return 'IV'
        elif self.x != 0 and self.y == 0:
             return "{} se sitúa sobre el eje X"
        elif self.x == 0 and self.y != 0:
            return "{} se sitúa sobre el eje Y"
        else:
             return "sobre el origen"
        return self.x and self.y

    def vector(self, p):
        v= Punto(p.x - self.x, p.y - self.y)
        return v

    def distancia(self, p):
        d = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2 )
        return d

    def __str__(self):
        return "({}, {})".format(self.x, self.y)

class Rectángulo:
    punto_inicial= None
    punto_final= None

    def __init__(self, punto_inicial, punto_final):
        self.punto_inicial = punto_inicial
        self.punto_final = punto_final

    def base(self):
        return self.punto_final.x - self.punto_inicial.x

    def altura(self):
        return self.punto_final.y - self.punto_inicial.y

    def área(self):
        return  self.base()*self.altura()

# Plano cartesiano

def dibujar_plano_carteciano(centro):
    xini=[0,centro[1]]
    xfin=[ancho,centro[1]]
    yini=[centro[0],0]
    yfin=[centro[0],alto]
    pygame.draw.line(pantalla,morado,xini,xfin)
    pygame.draw.line(pantalla,morado,yini,yfin)
    pygame.draw.rect(pantalla, turquesa, [700, 100, 6, 7], )
    pygame.draw.rect(pantalla, naranja, ((500, 600), (-300, -200)), 4)
    pygame.draw.rect(pantalla, verde, [500, 400, 5, 6])

# Puntos

    A = Punto(2,3)
    B = Punto(5,5)
    C = Punto(-3, -1)
    D = Punto(0,0)

# Cuadrante

    fuente = pygame.font.Font(None, 18)
    texto= fuente.render(f"El punto {A} se encuentra en el cuadrante {A.cuadrante()}", True, mangenta )
    pantalla.blit(texto, [680, 85])
    texto= fuente.render(f"El punto {C} se encuentra en el cuadrante {C.cuadrante()}", True, mangenta)
    pantalla.blit(texto, [200, 610])
    texto= fuente.render(f"El punto {D} se encuentra {D.cuadrante()}", True, mangenta)
    pantalla.blit(texto, [530, 400])

# Rectángulo

    rect= Rectángulo(A,B)
    """texto= fuente.render("La base del rectángulo es {}".format(rect.base()), True, morado)
    pantalla.blit(texto, [275,450])
    texto= fuente.render("La altura del rectángulo es {}".format(rect.altura()),True, morado)
    pantalla.blit(texto, [275, 495])
    texto= fuente.render("El área del rectángulo es {}".format(rect.área ()), True, morado)
    pantalla.blit(texto, [275, 540])"""


if  __name__== '__main__':

    pygame.init()
    pantalla=pygame.display.set_mode([ancho,alto])
    pantalla.fill(color=(234, 211, 211))
    dibujar_plano_carteciano(centro)
    pygame.display.flip()
    fuente=pygame.font.Font(None,25)

    fin=False
    while not fin:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                fin = True